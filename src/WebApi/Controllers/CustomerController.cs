using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Servicec;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomer cs;

        public CustomerController(ICustomer cs)
        {
            this.cs = cs;
        }


        [HttpGet("{id:long}")]   
        public ActionResult<Customer> GetCustomerAsync([FromRoute] long id)
        {
            Customer customer = cs.Get(id);
            if (customer == null)
            {
                return BadRequest("User is not found");
            }
            return Ok(customer);
        }

        [HttpPost("")]
        public ActionResult<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            long? id = cs.Set(customer);
            switch (id)
            {
                case null:
                    return BadRequest("error");
                case -1:
                    return Conflict("This user already exists");
                default:
                    return Ok(id);
            }
        }
    }
}