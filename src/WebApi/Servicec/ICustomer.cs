using WebApi.Models;

namespace WebApi.Servicec
{
    public interface ICustomer
    {
        public Customer Get(long id);

        public long? Set(Customer customer);
    }
}
