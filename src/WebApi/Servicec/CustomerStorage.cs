using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Servicec
{
    public class CustomerStorage : DbContext
    {
        public CustomerStorage(DbContextOptions options) : base(options) { }
        public DbSet<Customer> Customers { get; set; }
    }
}
